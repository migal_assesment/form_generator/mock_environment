# Mock environment

clone this project localy then follow the below instructions

## Server 1
### Run 
`docker run -d --name server1 -v $(pwd)/csv:/usr/share/nginx/html -p 8081:80 nginx:alpine`

### Use

after running successfully drop a csv file into the csv folder.

### Test if it is running correctly
copy the url http://localhost:8081/myfilename.csv into browser search.

 example if I dropped a file named <span style="color:#ADD8E6"><b>test.csv</b></span> in to the csv file I can go to the browser and put in the url http://localhost:8081/test.csv the file should say downloading

## Server 2

### build

`docker build -t express-upload-app .`

### Run 

`docker run -d --name server2 -v $(pwd)/pdfForms:/usr/src/app/uploads -p 3000:3000 express-upload-app`

### Use

after running successfully you can post a pdf file to the server.

### Test if it is running correctly

run following command
`curl -F "file=@/home/user/location/to/file/myPdf.pdf" http://localhost:3000/upload`

change <span style="color:#ADD8E6"><b>/home/user/location/to/file/myPdf.pdf</b></span> to your file name


## S3

### run 

`docker run -d -p 9000:9000 -p 9001:9001 --name s3-minio1 -v $(pwd)/s3:/data -e "MINIO_ROOT_USER=momentum" -e "MINIO_ROOT_PASSWORD=5behk27h" minio/minio server /data --console-address ":9001"`
