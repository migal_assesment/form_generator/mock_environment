const express = require('express');
const multer  = require('multer');
const path = require('path');
const fs = require('fs');

const UPLOAD_FOLDER = './uploads';
const app = express();
const port = 3000;

fs.mkdirSync(UPLOAD_FOLDER, { recursive: true });

app.use(express.raw({ type: 'application/pdf', limit: '50mb' }));

app.post('/upload', (req, res) => {
  // Check if the incoming request is of type application/pdf
  if (req.headers['content-type'] === 'application/pdf') {
    const filename = `uploaded-${Date.now()}.pdf`;
    const filepath = path.join(UPLOAD_FOLDER, filename);

    // Write the raw bytes from the request body to a file
    fs.writeFile(filepath, req.body, (err) => {
      if (err) {
        return res.status(500).json({ message: 'Error saving the file', error: err.message });
      }
      res.status(200).json({ message: 'File uploaded successfully', filename });
    });
  } else {
    res.status(400).json({ message: 'Invalid content type. Only application/pdf is allowed.' });
  }
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});

